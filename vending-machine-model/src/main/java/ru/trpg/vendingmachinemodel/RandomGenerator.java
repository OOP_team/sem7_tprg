package ru.trpg.vendingmachinemodel;

import java.util.Random;

class RandomGenerator extends Random {

	private int seed;

	RandomGenerator(int seed) {
		super(seed);
	}

	int generatePoisson(double lambda) {
	    // https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables
		assert lambda > 0;
		double L = Math.exp(-lambda);
        double p = 1.0;
        int k = 0;

        do {
            k++;
            p *= this.nextDouble();
        } while (p > L);

        return k - 1;
	}

	double generateGaussian(double mean, double sigma) {
        return this.nextGaussian()*(sigma*sigma) + mean;
	}

}