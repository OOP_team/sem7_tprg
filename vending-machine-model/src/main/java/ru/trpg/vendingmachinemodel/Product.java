package ru.trpg.vendingmachinemodel;

class Product {

	private String name;
	private int cost;
	private int maxAmount;

	Product(String name, int cost, int maxAmount) {
		this.name = name;
		this.cost = cost;
		this.maxAmount = maxAmount;
	}

	int getCost() {
		return this.cost;
	}

	int getMaxAmount() {
		return this.maxAmount;
	}

	String getName() {
		return this.name;
	}

}