package ru.trpg.vendingmachinemodel;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.ArrayList;

@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
public class VendingMachineModelBuilder {

	private void checkPositiveArgument(double value, String name) {
		if (value < 0) {
			throw new IllegalArgumentException("'" + name + "' argument should be positive!");
		}
	}

	private void checkProduct(Product product) {
		if (product.getCost() < 0) {
			throw new IllegalArgumentException(
					String.format("Product '%s' have negative cost!", product.getName())
			);
		}
		if (product.getMaxAmount() <= 0) {
			throw new IllegalArgumentException(
					String.format("Product '%s' have non-positive max amount!", product.getName())
			);
		}
	}
	
	private int seed = 42;

	private int maxTicksAmount = -1;
	private double clientsToServeLambda = 0.7;
	private double clientsIncomingLambda = 0.5;
	private double clientMoneyMean = 7;
	private double clientMoneyVariance = 1;
	private int maintananceTicks = 3;
	private int maintananceDelayTicks = 3;
	
	private ArrayList<Product> productArray = new ArrayList<Product>() {{
				add(new Product("product1", 1, 10));
				add(new Product("product2", 2, 10));
				add(new Product("product3", 3, 10));
				add(new Product("product4", 4, 10));
				add(new Product("product5", 5, 10));
		}};

	public VendingMachineModelBuilder() { }
	
	public VendingMachineModel build() {
		return new VendingMachineModel(
			seed,
			maxTicksAmount,
			clientsToServeLambda,
			clientsIncomingLambda,
			clientMoneyMean,
			clientMoneyVariance,
			maintananceTicks,
			maintananceDelayTicks,
			productArray
		);
	}
	
	public VendingMachineModelBuilder seed(int seed) {
		this.seed = seed;
		return this;
	}
	
	public VendingMachineModelBuilder maxTicksAmount(int maxTicksAmount) {
		if (maxTicksAmount == 0) {
			throw new IllegalArgumentException("'maxTicksAmount' should not be zero!");
		}
		this.maxTicksAmount = maxTicksAmount;
		return this;
	}
	
	public VendingMachineModelBuilder clientsToServeLambda(double clientsToServeLambda) {
		checkPositiveArgument(clientsToServeLambda, "clientsToServeLambda");
		this.clientsToServeLambda = clientsToServeLambda;
		return this;
	}
	
	public VendingMachineModelBuilder clientsIncomingLambda(double clientsIncomingLambda) {
		checkPositiveArgument(clientsIncomingLambda, "clientsIncomingLambda");
		this.clientsIncomingLambda = clientsIncomingLambda;
		return this;
	}
	
	public VendingMachineModelBuilder clientMoneyMean(double clientMoneyMean) {
		checkPositiveArgument(clientMoneyMean, "clientMoneyMean");
		this.clientMoneyMean = clientMoneyMean;
		return this;
	}
	
	public VendingMachineModelBuilder clientMoneyVariance(double clientMoneyVariance) {
		checkPositiveArgument(clientMoneyVariance, "clientMoneyVariance");
		this.clientMoneyVariance = clientMoneyVariance;
		return this;
	}

	public VendingMachineModelBuilder maintananceTicks(int maintananceTicks) {
		checkPositiveArgument(maintananceTicks, "maintananceTicks");
		this.maintananceTicks = maintananceTicks;
		return this;
	}
	
	public VendingMachineModelBuilder maintananceDelayTicks(int maintananceDelayTicks) {
		checkPositiveArgument(maintananceDelayTicks, "maintananceDelayTicks");
		this.maintananceDelayTicks = maintananceDelayTicks;
		return this;
	}
	
	public VendingMachineModelBuilder productArray(ArrayList<Product> productArray) {
		assert productArray.size() == 5;
		for (Product product : productArray)
			checkProduct(product);
		this.productArray = productArray;
		return this;
	}

}