package ru.trpg.vendingmachinemodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

class MaintenanceStation {

	private ServiceWorker currentServiceWorker;

	@JsonIgnore
	private VendingMachineModel vendingMachineModel;

	MaintenanceStation(VendingMachineModel vendingMachineModel) {
		this.vendingMachineModel = vendingMachineModel;
		this.currentServiceWorker = null;
	}

	void receiveServiceWorker(VendingMachine vm) {
		if (this.currentServiceWorker == null) {
			this.currentServiceWorker = new ServiceWorker(
					vendingMachineModel.MAINTANANCE_DELAY_TICKS,
					vendingMachineModel.MAINTANANCE_TICKS,
					vm,
					this);
		}
	}

	void tick() {
		if (this.currentServiceWorker != null) {
			this.currentServiceWorker.tick();
		}
	}

	void killWorker() {
		this.currentServiceWorker = null;
	}

	public ServiceWorker getServiceWorker() {
		return currentServiceWorker;
	}
}