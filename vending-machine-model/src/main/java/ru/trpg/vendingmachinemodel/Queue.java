package ru.trpg.vendingmachinemodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.LinkedList;

class Queue{

	private LinkedList<Client> clients; // TODO: выписывать только количество человек в очереди

	@JsonIgnore
	private VendingMachineModel vendingMachineModel;

	Queue(VendingMachineModel vendingMachineModel) {
		this.clients = new LinkedList<Client>();
		this.vendingMachineModel = vendingMachineModel;
	}

	void generateNewClients(RandomGenerator random) {
		int newClientsAmount = random.generatePoisson(vendingMachineModel.CLIENTS_INCOMING_LAMBDA);
		for (int i=0; i < newClientsAmount; i++) {
			Client newClient = Client.generate(this.vendingMachineModel);
			clients.push(newClient);
		}
	}

	public int getQueueSize() {
		return this.clients.size();
	}

	LinkedList<Client> getClients(){
		return clients;
	}

	Client popFirst() {
		return clients.poll();
	}

}