package ru.trpg.vendingmachinemodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

class ProductSlot {

	private Product product;
	private int amount;

	@JsonIgnore
    private VendingMachineModel vendingMachineModel;

	ProductSlot(Product product) {
		this.product = product;
		this.amount = product.getMaxAmount();
	}

	void refill() {
		this.amount = product.getMaxAmount();
	}

	boolean giveOutProduct() {
		if (this.amount > 0) {
			this.amount--;
			return true;
		} else {
			return false;
		}
	}

	boolean isEmpty() {
		return this.amount == 0;
	}

	public Product getProduct() {
		return this.product;
	}

	@JsonIgnore
	public String getPrintable() {
		int maxAmount = this.product.getMaxAmount();
		return String.format("Product '%s"+String.valueOf(maxAmount).length()+"' : %d/%d",
			this.product.getName(),
			this.amount,
			maxAmount
		);
	}

}