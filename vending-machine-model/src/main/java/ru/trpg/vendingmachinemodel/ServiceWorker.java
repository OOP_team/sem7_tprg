package ru.trpg.vendingmachinemodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

class ServiceWorker {

	private int ticksUntilDestination;
	private int ticksUntilMaintenanceEnd;
	@JsonIgnore
	private VendingMachine vendingMachine;
	@JsonIgnore
	private MaintenanceStation maintenanceStation;

	ServiceWorker(
			int ticksUntilDestination,
			int ticksUntilMaintenanceEnd,
			VendingMachine vendingMachine,
			MaintenanceStation maintenanceStation
	) {
		this.ticksUntilDestination = ticksUntilDestination;
		this.ticksUntilMaintenanceEnd = ticksUntilMaintenanceEnd;
		this.vendingMachine = vendingMachine;
		this.maintenanceStation = maintenanceStation;
	}

	private void startMaintenance() {
		vendingMachine.setIsWorking(false);
	}

	private void endMaintenance() {
		vendingMachine.refillProducts();
		vendingMachine.setIsWorking(true);
	}

	private void makeTravelTick() {
		if (this.ticksUntilDestination > 1) {
			ticksUntilDestination--;
		} else {
			ticksUntilDestination = 0;
			startMaintenance();
		}
	}

	private void makeMaitenanceTick() {
		if (this.ticksUntilMaintenanceEnd > 1) {
			this.ticksUntilMaintenanceEnd--;
		} else {
			ticksUntilMaintenanceEnd = 0;
			endMaintenance();
		}
	}

	void tick() {
		if (ticksUntilDestination > 0){
			this.makeTravelTick();
		} else if (ticksUntilMaintenanceEnd > 0) {
			this.makeMaitenanceTick();
		} else {
			maintenanceStation.killWorker();
		}
	}

	public int getTicksUntilDestination() {
		return ticksUntilDestination;
	}

	public int getTicksUntilMaintenanceEnd() {
		return ticksUntilMaintenanceEnd;
	}
}