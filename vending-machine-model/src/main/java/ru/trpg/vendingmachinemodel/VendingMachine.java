package ru.trpg.vendingmachinemodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

class VendingMachine {

	@JsonIgnore
    private VendingMachineModel vendingMachineModel;

	private MaintenanceStation maintenanceStation;

	private Queue queue;
	private ArrayList<ProductSlot> slots;
	private int totalRevenue;
	private int currentMoney;
	private boolean isWorking;

	VendingMachine(ArrayList<Product> products, VendingMachineModel vendingMachineModel) {
		queue = new Queue(vendingMachineModel);
		this.slots = products.stream()
				.map(product -> new ProductSlot(product))
                .collect(Collectors.toCollection(ArrayList::new));
		this.isWorking = true;
		this.vendingMachineModel = vendingMachineModel;
		this.maintenanceStation = new MaintenanceStation(this.vendingMachineModel);
	}

	void sendMaintenanceSignal() {
		this.maintenanceStation.receiveServiceWorker(this);
	}

	void refillProducts() {
		for (ProductSlot slot : slots) {
			slot.refill();
		}
	}

	public boolean getIsWorking() {
		return this.isWorking;
	}

	public ArrayList<ProductSlot> getSlots() {
		return this.slots; 
	}

	void setIsWorking(boolean b) {
		this.isWorking = b;
	}

	void enterMoney(int money) {
		this.currentMoney += money;
	}

	private void checkSlot(ProductSlot slot) {
		if (slot.isEmpty()) {
			this.sendMaintenanceSignal();
		}
	}

	boolean makePurchase(ProductSlot slot){
		if (
			slot.getProduct().getCost() <= currentMoney &&
			slot.giveOutProduct()
		) {
			int productCost = slot.getProduct().getCost();
			currentMoney -= productCost;
			this.totalRevenue += productCost;
			checkSlot(slot);
			return true;
		} else {
			return false;
		}
	}

	int returnChange() {
		int change = this.currentMoney;
		this.currentMoney = 0;
		return change;
	}

	public Queue getQueue() {
		return this.queue;
	}

    MaintenanceStation getMaintenanceStation() {
		return this.maintenanceStation;
	}

	public int getTotalRevenue() {
		return  this.totalRevenue;
	}

	public ServiceWorker getServiceWorker() {
	    return this.maintenanceStation.getServiceWorker();
    }

}