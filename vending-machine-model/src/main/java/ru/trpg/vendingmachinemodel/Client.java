package ru.trpg.vendingmachinemodel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Arrays;

class Client {

	private int money;
	private Product desiredProduct;

    @JsonIgnore
	private VendingMachineModel vendingMachineModel;

	private Client(int money, Product product) {
		this.money = money;
		this.desiredProduct = product;
	}

	static Client generate(VendingMachineModel vendingMachineModel) {
		int money = (int) vendingMachineModel
				.getRandom()
				.generateGaussian(
						vendingMachineModel.CLIENT_MONEY_MEAN,
						vendingMachineModel.CLIENT_MONEY_VARIANCE
				);
		if (money < 0) {
			money = 0;
		}
        ArrayList<Product> availableProducts = vendingMachineModel.PRODUCT_ARRAY;
		Product chosenProduct = makeProductChoice(
				vendingMachineModel.getRandom(),
				availableProducts
		);
		return new Client(money, chosenProduct);
	}

    private static Product makeProductChoice(RandomGenerator random, ArrayList<Product> productChoices) {
        Product chosenProduct = productChoices.get(random.nextInt(productChoices.size()));
        return chosenProduct;
    }

	boolean makePurchase(VendingMachine vm) {
	    this.enterMoney(vm);
        ProductSlot desiredSlot = Client.findSlot(vm, this.desiredProduct);
        boolean result = vm.makePurchase(desiredSlot);
        this.money = vm.returnChange();
        return result;
	}

	private static ProductSlot findSlot(VendingMachine vm, Product product) {
        return vm.getSlots().stream()
                .filter(slot -> slot.getProduct() == product)
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("Product '%s' not found in machine slots!", product.getName())));
    }

	private void enterMoney(VendingMachine vm) {
		vm.enterMoney(this.money);
		this.money = 0;
	}

}