package ru.trpg.vendingmachinemodel;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.util.ArrayList;

@JsonDeserialize(builder = VendingMachineModelBuilder.class)
public class VendingMachineModel implements Runnable {

	private VendingMachine vendingMachine;
	@JsonIgnore
	private RandomGenerator random;
	
	final int SEED;

	final int MAX_TICKS_AMOUNT;
	final double CLIENTS_TO_SERVE_LAMBDA;
	final double CLIENTS_INCOMING_LAMBDA;
	final double CLIENT_MONEY_MEAN;
	final double CLIENT_MONEY_VARIANCE;
	final int MAINTANANCE_TICKS;
	final int MAINTANANCE_DELAY_TICKS;
	
	final ArrayList<Product> PRODUCT_ARRAY;

	volatile private int tick;
	volatile private boolean started;
	volatile private boolean stopped;
    volatile boolean finished;

	boolean verbose; // for printing states

	VendingMachineModel(
			int seed,
			int maxTicksAmount,
			double clientsToServeLambda,
			double clientsIncomingLambda,
			double clientMoneyMean,
			double clientMoneyVariance,
			int maintananceTicks,
			int maintananceDelayTicks,
			ArrayList<Product> productArray
		) {

		this.SEED=seed;
		this.MAX_TICKS_AMOUNT=maxTicksAmount;
		this.CLIENTS_TO_SERVE_LAMBDA=clientsToServeLambda;
		this.CLIENTS_INCOMING_LAMBDA=clientsIncomingLambda;
		this.CLIENT_MONEY_MEAN=clientMoneyMean;
		this.CLIENT_MONEY_VARIANCE=clientMoneyVariance;
		this.MAINTANANCE_TICKS=maintananceTicks;
		this.MAINTANANCE_DELAY_TICKS=maintananceDelayTicks;
		this.PRODUCT_ARRAY=productArray;

		this.random = new RandomGenerator(SEED);
		this.vendingMachine = new VendingMachine(PRODUCT_ARRAY, this);

		this.started = false;
		this.stopped = false;
		this.finished= false;
		this.verbose = false;
		
	}

	private void tick() {

		this.vendingMachine.getQueue().generateNewClients(random);

		if (this.vendingMachine.getIsWorking()) {
			int peopleToService = random.generatePoisson(this.CLIENTS_TO_SERVE_LAMBDA);
			for (int i = 0; i < peopleToService; i++) {
				Client client = this.vendingMachine.getQueue().popFirst();
				if (client == null) {
					break;
				}
				client.makePurchase(this.vendingMachine);
			}
		}

		this.vendingMachine.getMaintenanceStation().tick();

	}
	
	public void run() {
		if (this.started) {
			System.out.println("Model already started!");
			return;
		}
		System.out.println("Starting...");
		this.tick = 1;
		this.started = true;
		while (!this.stopped && (MAX_TICKS_AMOUNT < 0 || this.tick <= MAX_TICKS_AMOUNT)) {
			this.tick();
			this.tick++; //FIXME: non-atomic
            if (this.verbose) {
                this.printState();
            }
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.finished = true;

		System.out.println("VendingMachineModel finished working");
	}
	
	public void stop() {
		System.out.println("Stopping..");
		this.stopped = true;
	}
	
	public void printState() {
		System.out.println(String.format("\n--- %d ---", tick));
		System.out.println(String.format("Queue size: %d", this.vendingMachine.getQueue().getClients().size()));
		System.out.println(String.format("Total revenue: %d", this.vendingMachine.getTotalRevenue()));
		System.out.println(String.format("Working: %b", this.vendingMachine.getIsWorking()));
		System.out.println("Current product slots:");
		for (ProductSlot slot : this.vendingMachine.getSlots()){
			System.out.println("\t" + slot.getPrintable());
		}
	}

	public void setVerbose(boolean verbose) {
	    this.verbose = verbose;
    }

	@JsonIgnore
	public String getState() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();

		mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);

		String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		System.out.println(jsonInString);

		return jsonInString;

    }
	
	@JsonIgnore
	public int getTick() {
		return this.tick;
	}
	
	@JsonIgnore
	public VendingMachine getVendingMachine() {
		return this.vendingMachine;
	}

    RandomGenerator getRandom() {
	    return this.random;
    }

    public boolean isStarted() {
		return this.started;
	}

	public boolean isFinished() {
	    return this.finished;
    }

    public boolean isStopped() {
	    return this.stopped;
    }

	public int getTotalRevenue() {return this.vendingMachine.getTotalRevenue(); }

	public int getQueueSize() { return this.vendingMachine.getQueue().getQueueSize(); }

	public boolean isMaintanenceNeeded() { return !this.vendingMachine.getIsWorking(); }

	public int getTicksUntilMaintenanceStart() { return this.vendingMachine.getServiceWorker().getTicksUntilDestination(); }

	public int getTicksUntilMaintenanceEnd() { return this.vendingMachine.getServiceWorker().getTicksUntilMaintenanceEnd(); }

}