package ru.trpg.vendingmachinemodel;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.hamcrest.core.StringEndsWith.endsWith;

import java.util.ArrayList;
import java.util.Random;

public class BuilderTest {

    static Random random = new Random();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testMaxTicksAmount() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("'maxTicksAmount' should not be zero!");

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.maxTicksAmount(0);
    }

    @Test
    public void testClientsToServeLambda() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("'clientsToServeLambda' argument should be positive!");

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        double value = -random.nextDouble() * random.nextInt(Integer.MAX_VALUE);
        builder.clientsToServeLambda(value);
    }

    @Test
    public void testClientsIncomingLambda() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("'clientsIncomingLambda' argument should be positive!");

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.clientsIncomingLambda(-random.nextDouble() * random.nextInt(Integer.MAX_VALUE));
    }

    @Test
    public void testClientMoneyMean() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("'clientMoneyMean' argument should be positive!");

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.clientMoneyMean(-random.nextDouble() * random.nextInt(Integer.MAX_VALUE));
    }

    @Test
    public void testClientMoneyVariance() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("'clientMoneyVariance' argument should be positive!");

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.clientMoneyVariance(-random.nextDouble() * random.nextInt(Integer.MAX_VALUE));
    }

    @Test
    public void testMaintananceTicks() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("'maintananceTicks' argument should be positive!");

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.maintananceTicks(-random.nextInt(Integer.MAX_VALUE));
    }

    @Test
    public void testMaintananceDelayTicks() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("'maintananceDelayTicks' argument should be positive!");

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.maintananceDelayTicks(-random.nextInt(Integer.MAX_VALUE));
    }

    @Test
    public void testWrongProductArray() {
        expectedException.expect(AssertionError.class);

        int arrayLength = -1;
        do {
            arrayLength = random.nextInt(25)+1;
        } while (arrayLength == 5);

        ArrayList<Product> productArray = new ArrayList<Product>();
        for (int i=0; i<arrayLength; i++) {
            productArray.add(ModelTest.randomProduct());
        }

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.productArray(productArray);
    }

    @Test
    public void testNegativeProductCost(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(endsWith(" have negative cost!")); //FIXME: expect full message

        ArrayList<Product> testProductArray = new ArrayList<Product>() {{
                add(ModelTest.randomProduct());
                add(ModelTest.randomProduct());
                add(ModelTest.randomProduct());
                add(ModelTest.randomProduct());
                add(new Product(
                        ModelTest.generateRandomString(),
                        -random.nextInt(Integer.MAX_VALUE)-1,
                        random.nextInt(Integer.MAX_VALUE)
                ));
        }};

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.productArray(testProductArray);
    }

    @Test
    public void testNegativeProductMaxAmount(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(endsWith(" have non-positive max amount!")); //FIXME: expect full message

        ArrayList<Product> testProductArray = new ArrayList<Product>() {{
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(new Product(
                    ModelTest.generateRandomString(),
                    random.nextInt(Integer.MAX_VALUE),
                    -random.nextInt(Integer.MAX_VALUE)-1
            ));
        }};

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.productArray(testProductArray);
    }

    @Test
    public void testZeroProductMaxAmount(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(endsWith(" have non-positive max amount!")); //FIXME: expect full message

        ArrayList<Product> testProductArray = new ArrayList<Product>() {{
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(new Product(
                    ModelTest.generateRandomString(),
                    random.nextInt(Integer.MAX_VALUE),
                    0
            ));
        }};

        VendingMachineModelBuilder builder = new VendingMachineModelBuilder();
        builder.productArray(testProductArray);
    }

    @Test
    public void testFullSuccessfulBuild() {
        Random rand = new Random();

        int seed = rand.nextInt();
        int maxTicksAmount = rand.nextInt(25000) + 5000;
        double clientsToServeLambda = rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE);
        double clientsIncomingLambda = rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE);
        double clientMoneyMean = rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE);
        double clientMoneyVariance = rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE);
        int maintananceTicks = rand.nextInt(Integer.MAX_VALUE);
        int maintananceDelayTicks = rand.nextInt(Integer.MAX_VALUE);

        ArrayList<Product> testProductArray = new ArrayList<Product>() {{
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
            add(ModelTest.randomProduct());
        }};

        VendingMachineModel model = new VendingMachineModelBuilder()
                .seed(seed)
                .maxTicksAmount(maxTicksAmount)
                .clientsToServeLambda(clientsToServeLambda)
                .clientsIncomingLambda(clientsIncomingLambda)
                .clientMoneyMean(clientMoneyMean)
                .clientMoneyVariance(clientMoneyVariance)
                .maintananceTicks(maintananceTicks)
                .maintananceDelayTicks(maintananceDelayTicks)
                .productArray(testProductArray)
                .build();
    }

}
