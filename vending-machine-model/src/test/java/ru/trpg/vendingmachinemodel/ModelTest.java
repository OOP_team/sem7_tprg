package ru.trpg.vendingmachinemodel;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.*;

public class ModelTest {

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_!?йцукенгшщзхъфывапролджэячсмитьб.";


    static String generateRandomString() {
        // https://stackoverflow.com/a/157202
        Random rand = new Random();
        int size = rand.nextInt(100);
        StringBuilder sb = new StringBuilder( size );
        for( int i = 0; i < size; i++ )
            sb.append( AB.charAt( rand.nextInt(AB.length()) ) );
        return sb.toString();
    }

    static Product randomProduct() {
        Random rand = new Random();
        return new Product(
                generateRandomString(),
                rand.nextInt(Integer.MAX_VALUE),
                rand.nextInt(Integer.MAX_VALUE)+1
        );
    }

    @Test
    public void testInterruption() {
        VendingMachineModel model = new VendingMachineModelBuilder()
                .maxTicksAmount(-1)
                .build();

        Thread modelThread = new Thread(model);
        modelThread.start();
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        modelThread.interrupt();
    }

    @Test
    public void fullyRandomModelRun() throws InterruptedException {

        Random rand = new Random();

        int seed = rand.nextInt();
        int maxTicksAmount = rand.nextInt(5000) + 5000;
        double clientsToServeLambda = rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE);
        double clientsIncomingLambda = rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE);
        double clientMoneyMean = rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE);
        double clientMoneyVariance = rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE);
        int maintananceTicks = rand.nextInt(Integer.MAX_VALUE);
        int maintananceDelayTicks = rand.nextInt(Integer.MAX_VALUE);

        ArrayList<Product> testProductArray = new ArrayList<Product>() {{
                add(randomProduct());
                add(randomProduct());
                add(randomProduct());
                add(randomProduct());
                add(randomProduct());
        }};

        VendingMachineModel model = new VendingMachineModelBuilder()
                .seed(seed)
                .maxTicksAmount(maxTicksAmount)
                .clientsToServeLambda(clientsToServeLambda)
                .clientsIncomingLambda(clientsIncomingLambda)
                .clientMoneyMean(clientMoneyMean)
                .clientMoneyVariance(clientMoneyVariance)
                .maintananceTicks(maintananceTicks)
                .maintananceDelayTicks(maintananceDelayTicks)
                .productArray(testProductArray)
                .build();

        Thread modelThread = new Thread(model);
        modelThread.start();

        while (model.getTick() < maxTicksAmount) {
            Thread.sleep(2000);
            System.out.println(String.format("Tick %d out of %d", model.getTick(), maxTicksAmount));
            model.printState();
        }
        assertNotNull(model.getVendingMachine());

        assertTrue(model.isStarted());
        assertFalse(model.isStopped());

    }



    @Test
    public void determinedModelRun() throws
            InterruptedException,
            JsonGenerationException,
            JsonMappingException,
            IOException {

        ArrayList<Product> testProductArray =  new ArrayList<Product>() {{
                add(new Product("product1", 1, 10));
                add(new Product("product2", 2, 10));
                add(new Product("product3", 3, 10));
                add(new Product("product4", 4, 10));
                add(new Product("product5", 5, 10));
        }};

        VendingMachineModel model = new VendingMachineModelBuilder()
                .seed(300)
                .maxTicksAmount(300)
                .clientsToServeLambda(10.5)
                .clientsIncomingLambda(20.5)
                .clientMoneyMean(10)
                .clientMoneyVariance(3)
                .maintananceTicks(3)
                .maintananceDelayTicks(3)
                .productArray(testProductArray)
                .build();
        model.setVerbose(true);

        assertNotNull(model.getVendingMachine());

        Thread modelThread = new Thread(model);
        modelThread.start();

        Thread.sleep(100);
        model.run();

        while (!model.isFinished()) {
            Thread.sleep(500);
        }

        assertEquals(301, model.getTick());
        assertEquals(4269, model.getTotalRevenue());
        assertEquals(4079, model.getQueueSize());

        boolean expectedMaintance = true;
        assertEquals(expectedMaintance, model.isMaintanenceNeeded());
        if (expectedMaintance) {
            assertEquals(0, model.getTicksUntilMaintenanceStart());
            assertEquals(1, model.getTicksUntilMaintenanceEnd());
        }

        String modelState = model.getState();

    }

    @Test
    public void defaultModelRun() throws InterruptedException {
        VendingMachineModel model = new VendingMachineModelBuilder().build();

        Thread modelThread = new Thread(model);
        modelThread.start();

        Thread.sleep(500);
        model.stop();
        Thread.sleep(100); // to ensure that model have time to stop

        assertTrue(model.isFinished());
        assertTrue(model.isStarted());
        assertTrue(model.isStopped());
    }

    @Test
    public void testPoissonCorrect() {
        Random rand = new Random();
        RandomGenerator randomGenerator = new RandomGenerator(0);
        int result = randomGenerator.generatePoisson(rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE));
        assertTrue(result >= 0);
    }

    @Test (expected = AssertionError.class)
    public void testPoissonZero() {
        RandomGenerator randomGenerator = new RandomGenerator(0);
        int result = randomGenerator.generatePoisson(0);
    }

    @Test (expected = AssertionError.class)
    public void testPoissonWrong() {
        Random rand = new Random();
        RandomGenerator randomGenerator = new RandomGenerator(0);
        int result = randomGenerator.generatePoisson(-rand.nextDouble() * rand.nextInt(Integer.MAX_VALUE));
    }

}
