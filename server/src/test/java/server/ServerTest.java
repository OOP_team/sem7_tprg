package server;

import com.fasterxml.jackson.databind.JsonMappingException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.trpg.server.HomeController;
import ru.trpg.server.Lab2Application;
import ru.trpg.server.VendingMachineModelController;

import java.util.Random;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Lab2Application.class)
@AutoConfigureMockMvc
public class ServerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HomeController homeController;

    @Autowired
    private VendingMachineModelController modelController;

    @Before
    public void removeCurrentVendingMachine() {
        VendingMachineModelController.vendingMachineModel = null;
    }


    @Test
    public void testContextLoads() throws Exception {
        assertThat(homeController).isNotNull();
        assertThat(modelController).isNotNull();
    }

    @Test
    public void testHomePage() throws Exception {
        this.mockMvc
                .perform(get("/home"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Модеь торгового автомата</title>")));
    }

    @Test
    public void testUnitializedModel() throws Exception {
        assertThat(VendingMachineModelController.vendingMachineModel).isNull();
        this.mockMvc
                .perform(post("/model/start"))
                .andExpect(status().isBadRequest());

        this.mockMvc
                .perform(post("/model/stop"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testModelReinitialization() throws Exception {
        assertThat(VendingMachineModelController.vendingMachineModel).isNull();
        // create actual model
        this.mockMvc
                .perform(post("/model"))
                .andExpect(status().isOk());
        assertThat(VendingMachineModelController.vendingMachineModel).isNotNull();

        // try to reinitialize model
        this.mockMvc
                .perform(post("/model"))
                .andExpect(status().isOk());
        assertThat(VendingMachineModelController.vendingMachineModel).isNotNull();
    }

    @Test
    public void testModelController() throws Exception {

        assertThat(VendingMachineModelController.vendingMachineModel).isNull();

        // create actual model
        this.mockMvc
                .perform(post("/model"))
                .andExpect(status().isOk());
        assertThat(VendingMachineModelController.vendingMachineModel).isNotNull();

        // start modeling process for a short time
        this.mockMvc
                .perform(put("/model/start"))
                .andExpect(status().isOk());
        Thread.sleep(100); //
        assertThat(VendingMachineModelController.vendingMachineModel.isStarted()).isTrue();

        Thread.sleep(1000);

        // stop modeling and wait model to stop
        this.mockMvc
                .perform(post("/model/stop"))
                .andExpect(status().isOk());
        Thread.sleep(100); //
        assertThat(VendingMachineModelController.vendingMachineModel.isStopped()).isTrue();
        assertThat(VendingMachineModelController.vendingMachineModel.isFinished()).isTrue();

        // check bad PUT-requests
        this.mockMvc
                .perform(post("/model/start"))
                .andExpect(status().isBadRequest());

        this.mockMvc
                .perform(post("/model/stop"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void testJsonInput() throws Exception {

        this.mockMvc
                .perform(
                    post("/model")
                            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                            .content("{\"seed\":654, \"maxTicksAmount\":50}")
                )
                .andExpect(status().isOk());
        assertThat(VendingMachineModelController.vendingMachineModel).isNotNull();

        // start modeling process for a short time
        this.mockMvc
                .perform(put("/model/start"))
                .andExpect(status().isOk());
        Thread.sleep(1500);

        assertThat(VendingMachineModelController.vendingMachineModel.isStopped()).isFalse();
        assertThat(VendingMachineModelController.vendingMachineModel.isFinished()).isTrue();
        assertThat(VendingMachineModelController.vendingMachineModel.getTick()).isEqualTo(51);

    }

    @Test
    public void testWrongJsonInput() throws Exception {

        this.mockMvc
                .perform(
                    post("/model")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("wrong input")
                )
                .andExpect(status().isBadRequest());

    }

    static final String AB = "0123456789";

    public static String generateLargeIntegerString() {
        Random rand = new Random();
        int size = rand.nextInt(100)+11;
        StringBuilder sb = new StringBuilder( size );
        sb.append( AB.charAt( rand.nextInt(AB.length()-1)+1 ) );
        for( int i = 0; i < size-1; i++ )
            sb.append( AB.charAt( rand.nextInt(AB.length()) ) );
        return sb.toString();
    }

    static final String[] INTEGER_PARAMETERS = {"seed", "maxTicksAmount", "maintananceTicks", "maintananceDelayTicks"};

    @Test
    public void testLargeIntegerValues() throws Exception {

        String value = generateLargeIntegerString();
        String key = INTEGER_PARAMETERS[new Random().nextInt(INTEGER_PARAMETERS.length)];

        this.mockMvc
                .perform(
                        post("/model")
                                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                                .content(String.format("{\"%s\":%s}", key, value))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString(
                        String.format("Numeric value (%s) out of range of int", value))));

    }

    @Test
    public void testMain() {
        Lab2Application.main(new String[] {});
    }

}