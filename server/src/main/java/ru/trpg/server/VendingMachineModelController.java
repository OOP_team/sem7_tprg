package ru.trpg.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.trpg.vendingmachinemodel.VendingMachineModel;

@RestController
@RequestMapping("/model")
public class VendingMachineModelController {

    public static VendingMachineModel vendingMachineModel;

    @PostMapping
    public ResponseEntity initializeModel(@RequestBody(required=false) String json) {
        if (json==null) {
            json="{}";
        }
        if (vendingMachineModel != null) {
            vendingMachineModel.stop();
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            vendingMachineModel = mapper.readValue(json, VendingMachineModel.class);
            return ResponseEntity.ok("{\"data\": \"Model initialized\"}");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }


    @PutMapping
    @RequestMapping("/start")
    public ResponseEntity  startModel() {
        if(vendingMachineModel == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Model is not initialized");
        } else if (vendingMachineModel.isStarted()) {
//            throw new IllegalArgumentException("Наверное, это ошибка");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Model already started");
        } else {
            Thread modelThread = new Thread(vendingMachineModel);
            modelThread.start();
            return ResponseEntity.ok("Model started");
        }
    }

    @PutMapping
    @RequestMapping("/stop")
    public ResponseEntity stopModel()
    {
        if (vendingMachineModel == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Model is not initialized");
        } else if (vendingMachineModel.isFinished()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Model already finished working");
        } else {
            vendingMachineModel.stop();
            return ResponseEntity.ok("Model stopped");
        }
    }

}